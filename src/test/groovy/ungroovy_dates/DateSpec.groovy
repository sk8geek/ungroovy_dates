package ungroovy_dates;

import spock.lang.Specification


class DateSpec extends Specification {

    void "test Groovy Date extensions"() {

        expect:
        new Date().toCalendar() instanceof Calendar

    }

}
